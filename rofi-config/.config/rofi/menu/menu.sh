#!/usr/bin/env bash

# Current Theme
dir="$HOME/.config/rofi/menu"
theme='style-dark'

## Esegui
rofi -show drun -theme ${dir}/${theme}.rasi

